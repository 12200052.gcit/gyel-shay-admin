# Gyel Shay Application
Gyalpozhing College of Information
BScIT, Second Year
Mobile Application project

It is for students and other interested candidates who are thrilled to learn Dzongkha and philosophies proposed and taught by prominent Buddhist masters.

Application built using JS/REACT-NATIVE/EXPO/FIREBASE/NODE.JS

(email: admin@gmail.com | password: admin22)

Admin Built using: JS/CSS/HTML, Node.js, Firebase, React.
## Images and videos

 [promotional video](https://www.youtube.com/watch?v=K_MX3eCsr0Y)

screenshots of the website:
<p float="left">
<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/admin%20login%20form.png?alt=media&token=39796991-cb69-4bd7-adf9-9f17983cefa5" width="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/admin%20dashbord.png?alt=media&token=b993dd13-a6cc-4462-8e2b-4afdb8564d5b" width="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/add%20stanza%20form.png?alt=media&token=425f7525-f655-46bf-b9be-c04459f32801" width="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/stanzalist.png?alt=media&token=29b057d6-501a-4090-95fc-0df81942deeb" width="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/add%20video%20form.png?alt=media&token=a7bbeb33-d572-4219-84d6-c4f19529a003" width="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/videolist.png?alt=media&token=008c3737-8d0a-4c2b-bab7-57e0ea7703bf" width="300"/>

</p>

screenshots of the Application:
<p float="left">
<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Fhome.jpg?alt=media&token=ca1146de-f6f0-401a-9a91-d68dd3748ea1" width="200" height="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Fintro.jpg?alt=media&token=1cd169d6-2e94-47a9-82b2-55f21235cf94" width="200" height="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Fstanzas.jpg?alt=media&token=22c65ab9-fdb3-4fbf-9b1b-8d3d0f8161c3" width="200" height="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Fstanza.jpg?alt=media&token=ed06c3f7-4188-4f36-8676-c38399ee8629" width="200" height="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Fvideo.jpg?alt=media&token=24837dfb-df1a-49bd-9f54-542a58684f8e3" width="200" height="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Flandscape%20video.jpg?alt=media&token=757f05d8-fdca-46cb-b670-21cea5073b2f" width="300" height="200"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Fquiz.jpg?alt=media&token=49e4641a-9d7a-41e1-9ca3-f857b6526c13" width="200" height="300"/>
</p>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Fquiz%20end.jpg?alt=media&token=b6bcea4e-d9f1-4fff-99a2-d3b690e68d9a" width="200" height="300"/>

<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/user%2Fabout%20us.jpg?alt=media&token=fd2dd8aa-48cf-429c-b58d-d0fe8a8c8946" width="200" height="300"/>
</p>

Final poster
<div>
<img src="https://firebasestorage.googleapis.com/v0/b/gyel-shay.appspot.com/o/itw202Poster.jpeg?alt=media&token=3281352e-370a-4dc0-b307-d2404ae46937" width="300" height="500"/>
</div>



.
