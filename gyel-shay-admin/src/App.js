import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/home/Home";
import Stanza from "./pages/stanza/Stanza";
import AddVideo from "./pages/videos/AddVideo";
import Login from "./pages/login/Login";
function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/">
            <Route index element={<Login />} />
            <Route path="home" element={<Home />} />
          </Route>
          <Route path="stanzas" element={<Stanza />}></Route>
          <Route path="video" element={<AddVideo />}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
